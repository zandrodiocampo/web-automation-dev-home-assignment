// declaring variables outside the function for easier access.
import fetch from "node-fetch";
let products;
let filteredProducts;
let minPrice = 100;
let maxPrice = 500;
let count;
let total;

// accessing the API using the fetch method
const fetchProducts = 
    fetch('https://api.ecommerce.com/products') //I used this dummy API to test if it is working "https://dummyjson.com/products"
    .then(res => res.json())
    .then(data => {
        
        products = Object.values(data); // object.values converts the object into array
        // console.log(products);


         console.log(Array.isArray(products)); //this checks if the product is in array format

        // the dummy API stores the products list in index[0], i only need to loop through index[0]
        filteredProducts = products[0].filter(function(product){
            // console.log(product.id)

            // I set the limits of the minimum price and the maximum price
            return product.price >= minPrice && product.price <= maxPrice;
        })
        
        console.log(filteredProducts); //logs the products within the price range
        
        count = filteredProducts.length;
        console.log("count: " + count); //number of products within the price range

        total = products[0].length;
        console.log("total: " + total); //total number of available products
        
    });

